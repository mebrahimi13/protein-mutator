package gui;

import java.awt.event.KeyEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.text.DefaultEditorKit;

public class ApplicationContainer extends JFrame{
	private static final long serialVersionUID = 1L;
	
	JTextArea textAre_sequence;
	JScrollPane scroll_sequence;
	JTextField textField_begin;
	JTextField textField_end;
	JTextField textField_number;
	JTextField textField_output;
	
	JLabel label_sequence;
	JLabel label_begin;
	JLabel label_end;
	JLabel label_number;
	JLabel label_output;
	
	JButton button_output;
	JButton button_start;
	JProgressBar progressBar;

	public ApplicationContainer()
    {
		initComponents();
		GroupLayout layout = createLayout();
		getContentPane().setLayout(layout);
		
		button_output.addActionListener(new BrowsButtonActionListener(this));
        button_start.addActionListener(new StartButtonActionListener(this));
        
        setTitle("Protein Mutator");
        setJMenuBar(createMenuBar());
        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
	
	private void initComponents()
	{
		label_sequence = new JLabel("Sequence:");
        textAre_sequence = new JTextArea(20,80);
        scroll_sequence = new JScrollPane(textAre_sequence);
        
        label_begin = new JLabel("Mutation Region Begin Index:");
        textField_begin = new JTextField(3);
        
        label_end = new JLabel("Mutation Region End Index:");
        textField_end = new JTextField(3);
        
        label_number = new JLabel("Number of Mutation(s) per Sequence:");
        textField_number = new JTextField(3);
        
        label_output = new JLabel("Output File:");
        textField_output = new JTextField(10);
        button_output = new JButton("Brows");
        
        button_start = new JButton("Start");
        
        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
	}
	
	private GroupLayout createLayout()
	{
		GroupLayout layout = new GroupLayout(this.getContentPane());
		layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        		.addComponent(label_sequence)
        		.addComponent(scroll_sequence)
        		.addGroup(layout.createSequentialGroup()
        				.addComponent(label_begin)
        				.addComponent(textField_begin)
        				.addComponent(label_end)
        				.addComponent(textField_end)
        				.addComponent(label_number)
        				.addComponent(textField_number))
        		.addGroup(layout.createSequentialGroup()
        				.addComponent(label_output)
        				.addComponent(textField_output)
        				.addComponent(button_output))
        		.addGroup(layout.createSequentialGroup()
        				.addComponent(button_start)
        				.addComponent(progressBar))
        				);
        
        layout.setVerticalGroup(layout.createSequentialGroup()
			 	.addComponent(label_sequence)
			 	.addComponent(scroll_sequence)
        		.addGroup(layout.createParallelGroup()
        				.addComponent(label_begin)
        				.addComponent(textField_begin)
        				.addComponent(label_end)
        				.addComponent(textField_end)
        				.addComponent(label_number)
        				.addComponent(textField_number))
        		.addGroup(layout.createParallelGroup()
        				.addComponent(label_output)
        				.addComponent(textField_output)
        				.addComponent(button_output))
        		.addGroup(layout.createParallelGroup()
        				.addComponent(button_start)
        				.addComponent(progressBar))
        				);
        
        return layout;
	}
	
    /**
     * Create an Edit menu to support cut/copy/paste.
     */
    public JMenuBar createMenuBar () {
        JMenuItem menuItem = null;
        JMenuBar menuBar = new JMenuBar();
        JMenu mainMenu = new JMenu("Edit");
        mainMenu.setMnemonic(KeyEvent.VK_E);
 
        menuItem = new JMenuItem(new DefaultEditorKit.CutAction());
        menuItem.setText("Cut");
        menuItem.setMnemonic(KeyEvent.VK_T);
        mainMenu.add(menuItem);
 
        menuItem = new JMenuItem(new DefaultEditorKit.CopyAction());
        menuItem.setText("Copy");
        menuItem.setMnemonic(KeyEvent.VK_C);
        mainMenu.add(menuItem);
 
        menuItem = new JMenuItem(new DefaultEditorKit.PasteAction());
        menuItem.setText("Paste");
        menuItem.setMnemonic(KeyEvent.VK_P);
        mainMenu.add(menuItem);
 
        menuBar.add(mainMenu);
        return menuBar;
    }
    
    public static void main(String args[])
    {
    	java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel(
                    		"javax.swing.plaf.metal.MetalLookAndFeel");
                            //  "com.sun.java.swing.plaf.motif.MotifLookAndFeel");
                            //UIManager.getCrossPlatformLookAndFeelClassName());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
				(new ApplicationContainer()).setVisible(true);
            }
        });
    }
	
}
