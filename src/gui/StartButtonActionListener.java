package gui;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigInteger;

import javax.swing.JOptionPane;

import combination.MathUtil;

public class StartButtonActionListener implements ActionListener {
	
	private ApplicationContainer applicationContainer;
	private ConfigurationValidator configValidator;

	public StartButtonActionListener(final ApplicationContainer applicationContainer)
	{
		this.applicationContainer = applicationContainer;
		this.configValidator = new ConfigurationValidator(applicationContainer);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(applicationContainer.button_start))
		{
			try
			{
				handleStartButton();
			}
			catch(IllegalArgumentException exp)
			{
				JOptionPane.showMessageDialog(
					applicationContainer,
				    exp.getMessage(),
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
			}
			catch(Exception exp)
			{
				JOptionPane.showMessageDialog(
					applicationContainer,
				    "Unexpected error occured: " + exp.getMessage(),
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	private void handleStartButton()
	{
		final ExecutionCofiguration executionConfiguration = configValidator.validateAndReturnConfiguration();
			
		BigInteger totalGenerated = MathUtil.mutationCount(executionConfiguration);
			
		int shouldContintue = JOptionPane.showConfirmDialog(
			    applicationContainer,
			    "This will generate " + totalGenerated.toString() + " sequences.\nDo you want to continue?",
			    "",
			    JOptionPane.YES_NO_OPTION);
			
		if(shouldContintue == JOptionPane.YES_OPTION)
		{
			applicationContainer.button_start.setEnabled(false);
			applicationContainer.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			UIWorker uiWorker = new UIWorker(applicationContainer, executionConfiguration);
			uiWorker.addPropertyChangeListener(
				     new PropertyChangeListener() {
				         public  void propertyChange(PropertyChangeEvent evt) {
				             if ("progress".equals(evt.getPropertyName())) {
				            	 applicationContainer.progressBar.setValue((Integer)evt.getNewValue());
				             }
				         }
				     });
			uiWorker.execute();
		}
	}
}
