package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

public class BrowsButtonActionListener implements ActionListener {
	
	private ApplicationContainer proteinMutator;
	private JFileChooser fileChooser;

	public BrowsButtonActionListener(final ApplicationContainer proteinMutator)
	{
		this.proteinMutator = proteinMutator;
		fileChooser = new JFileChooser();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(proteinMutator.button_output)){
			handleOutputButton();
		}
	}

	private void handleOutputButton() {
		int returnValue = fileChooser.showOpenDialog(proteinMutator);
		if (returnValue == JFileChooser.APPROVE_OPTION) 
		{
            File file = fileChooser.getSelectedFile();
            proteinMutator.textField_output.setText(file.getAbsolutePath());
		}
	}
}
