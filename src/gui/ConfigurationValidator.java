package gui;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import protein.AminoAcid;
import protein.Sequence;

public class ConfigurationValidator {

	private ApplicationContainer applicationContainer;

	public ConfigurationValidator(final ApplicationContainer applicationContainer) 
	{
		this.applicationContainer = applicationContainer;
	}
	
	public ExecutionCofiguration validateAndReturnConfiguration()
	{
		Sequence sequence = validateAndReturnSequence();
		int begin = validateAndReturnBeginIndex();
		int end = validateAndReturnEndIndex();
		int mutationNumber = validateAndReturnMutationNumber();
		BufferedWriter output = validateAndReturnOutputFile();
		
		return new ExecutionCofiguration(sequence, begin, end, mutationNumber, output);
	}
	
	private Sequence validateAndReturnSequence() {
		String input = applicationContainer.textAre_sequence.getText();
		
		if( input.length() == 0)
		{
			throw new IllegalArgumentException("Please provide a protein sequence to be mutated.");
		}
		
		return AminoAcid.createAminoAcidSequence(input);
	}
	
	private int validateAndReturnSequenceLength()
	{
		String input = applicationContainer.textAre_sequence.getText();
		
		if( input.length() == 0)
		{
			throw new IllegalArgumentException("Please provide a protein sequence to be mutated.");
		}
		return input.length();
	}
	
	private int validateAndReturnBeginIndex() {
		String text = applicationContainer.textField_begin.getText();
		int sequenceLength = validateAndReturnSequenceLength();
		
		if( text.length() == 0 )
		{
			throw new IllegalArgumentException("Please provide a value for the mutation begin index.");
		}
		
		int value;
		try
		{
			value = Integer.valueOf(text);
		}
		catch(NumberFormatException exp)
		{
			throw new IllegalArgumentException("The mutation begin index must be an integer.");
		}
		
		if(  value < 1 )
		{
			 throw new IllegalArgumentException("The mutation begin index can not be smaller than 1.");
		}
		
		if( value > sequenceLength )
		{
			throw new IllegalArgumentException("The mutation begin index can not be bigger than the length of the sequence, " 
					+ String.valueOf(sequenceLength) + ".");
		}
		
		return value;
	}
	
	private int validateAndReturnEndIndex() {
		String text = applicationContainer.textField_end.getText();
		int sequenceLength = validateAndReturnSequenceLength();
		
		if( text.length() == 0 )
		{
			throw new IllegalArgumentException("Please provide a value for the mutation end index.");
		}
		
		int value;
		try
		{
			value = Integer.valueOf(text);
		}
		catch(NumberFormatException exp)
		{
			throw new IllegalArgumentException("The mutation end index must be an integer.");
		}
		
		if(  value < 1 )
		{
			 throw new IllegalArgumentException("The mutation end index can not be smaller than 1.");
		}
		
		if( value > sequenceLength )
		{
			throw new IllegalArgumentException("The mutation end index can not be bigger than the length of the sequence, " 
					+ String.valueOf(sequenceLength) + ".");
		}
		
		return value;
		
	}
	
	private int validateAndReturnMutationNumber() {
		int begin = validateAndReturnBeginIndex();
		int end = validateAndReturnEndIndex();
		
		if( begin > end )
		{
			throw new IllegalArgumentException("The mutation begin index must be smaller than or equal to the end index.");
		}
		
		String text = applicationContainer.textField_number.getText();
		
		if( text.length() == 0 )
		{
			throw new IllegalArgumentException("Please provide a value for the number of mutations.");
		}
		
		int value;
		try
		{
			value = Integer.valueOf(text);
		}
		catch(NumberFormatException exp)
		{
			throw new IllegalArgumentException("The number of mutations must be an integer.");
		}
		
		int mutationRegionLenght = end - begin + 1;
		if(  mutationRegionLenght < value )
		{
			throw new IllegalArgumentException("The length of the mutation regin specified by begin and end indexes can not be shorter than the number of mutations.");
		}
		
		return value;
	}
	
	private BufferedWriter validateAndReturnOutputFile() {
		String text = applicationContainer.textField_output.getText();
		
		if( text.length() == 0 )
		{
			throw new IllegalArgumentException("Please provide a value for the output file.");
		}
		
		BufferedWriter outputFileWriter;
		try {
			outputFileWriter = new BufferedWriter(new FileWriter(text));
		} 
		catch (IOException e) {
			throw new IllegalArgumentException("Can not write to the provided output file.");
		}
		
		return outputFileWriter;
	}
}
