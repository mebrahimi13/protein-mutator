package gui;

import java.io.BufferedWriter;

import protein.Sequence;

public class ExecutionCofiguration 
{
	private final Sequence sequence;
	private final int begin;
	private final int end;
	private final int mutationNumber;
	private final BufferedWriter output;
	
	public ExecutionCofiguration(Sequence sequence, int begin, int end,
			int mutationNumber, BufferedWriter output) 
	{
		this.begin = begin;
		this.end = end;
		this.mutationNumber = mutationNumber;
		this.output = output;
		this.sequence = sequence;
	}

	public Sequence getSequence() {
		return sequence;
	}

	public int getBegin() {
		return begin;
	}

	public int getEnd() {
		return end;
	}

	public int getMutationNumber() {
		return mutationNumber;
	}

	public BufferedWriter getOutput() {
		return output;
	}
}
