package gui;

import java.io.IOException;
import java.math.BigInteger;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import combination.SequenceMutator;
import combination.MathUtil;

public class UIWorker extends SwingWorker<Object, Object> {
	private ApplicationContainer applicationContainer;
	private final SequenceMutator combination;
	private ExecutionCofiguration configuration;
	
	private final int totalToBeGenerated;
	private int totalGenerated;
	
	public UIWorker(final ApplicationContainer applicationContainer, final ExecutionCofiguration configuration)
	{
		this.applicationContainer = applicationContainer;
		this.combination = new SequenceMutator(this, configuration);
		this.configuration = configuration;
		this.totalToBeGenerated = MathUtil.mutationCount(configuration).intValue();
		totalGenerated = 0;
	}
	
	public void addProgress(BigInteger bigInteger)
	{
		totalGenerated += bigInteger.intValue();
		double progress = 100.0 * totalGenerated / totalToBeGenerated;
		this.setProgress((int)progress);
	}
	
	@Override
	protected Object doInBackground() throws Exception {
		totalGenerated = 0;
		setProgress(0);
		combination.compute();
		return null;
	}

	@Override
    protected void done() {
		applicationContainer.button_start.setEnabled(true);
		applicationContainer.setCursor(null);
		try {
			configuration.getOutput().close();
			JOptionPane.showMessageDialog(applicationContainer, "Done!");
			
		} catch (IOException exp) {
			JOptionPane.showMessageDialog(
					applicationContainer,
				    exp.getMessage(),
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
		}
	}
}
