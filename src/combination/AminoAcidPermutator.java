package combination;

import gui.ExecutionCofiguration;

import java.io.IOException;
import java.util.List;

import protein.AminoAcid;
import protein.Sequence;

public class AminoAcidPermutator{
	
	private ExecutionCofiguration configuration;
	private Stack<AminoAcid> stack;
	
	public AminoAcidPermutator(final ExecutionCofiguration configuration)
	{
		this.configuration = configuration;
		this.stack = new Stack<AminoAcid>();
	}
	
	public void permutate(final List<Integer> substitutionLocations)
	{
		stack.clear();
		compute(0, substitutionLocations);
	}
	
	private void compute(int substitutionLocationIndex, List<Integer> substitutionLocations)
	{
		if(substitutionLocationIndex < substitutionLocations.size() - 1)
		{
			handleRecursion(substitutionLocationIndex, substitutionLocations);
		}
		else
		{
			handleSimpleCase(substitutionLocationIndex, substitutionLocations);
		}
	}

	private void handleRecursion(int substitutionLocationIndex, List<Integer> substitutionLocations) 
	{
		int substitutionLocation = substitutionLocations.get(substitutionLocationIndex);
		AminoAcid aminoAcid = configuration.getSequence().getAminoAcidAt(substitutionLocation);
		for(AminoAcid toSubstitue : AminoAcid.values())
		{
			if(toSubstitue != aminoAcid)
			{
				stack.push(toSubstitue);
				compute(substitutionLocationIndex+1, substitutionLocations);
				stack.pop();
			}
		}
	}

	private void handleSimpleCase(int substitutionLocationIndex, List<Integer> substitutionLocations) 
	{
		int substitutionLocation = substitutionLocations.get(substitutionLocationIndex);
		AminoAcid aminoAcid = configuration.getSequence().getAminoAcidAt(substitutionLocation);
		for(AminoAcid toSubstitue : AminoAcid.values())
		{
			if(toSubstitue != aminoAcid)
			{
				List<AminoAcid> substitutions = stack.readAll();
				substitutions.add(toSubstitue);
				writeMutatedProtein(substitutionLocations, substitutions);
			}
		}
	}
	
	private void writeMutatedProtein(List<Integer> mutationLocations, List<AminoAcid> substitutions) 
	{
		// System.out.println("Substituting at " + mutationLocations + " with " + substitutions);
		Sequence mutatedSequence = new Sequence(configuration.getSequence());
		for(int ind = 0 ; ind < mutationLocations.size(); ind++)
		{
			int location = mutationLocations.get(ind);
			AminoAcid substitution = substitutions.get(ind);
			mutatedSequence.substitute(location, substitution);
		}
		try 
		{
			configuration.getOutput().write(mutatedSequence + "\n");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
}
