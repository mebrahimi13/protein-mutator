package combination;

import java.util.ArrayList;
import java.util.List;

public final class Stack<T extends Object> {
	
	private List<T> stack;
	
	public Stack()
	{
		this.stack = new ArrayList<T>();
	}

	public void push(T value)
	{
		stack.add(value);
	}
	
	public T pop()
	{
		if(stack.isEmpty())
		{
			throw new IllegalStateException("Pop is called on empty stack!");
		}
		
		int lastIndex = stack.size() - 1;
		return stack.remove(lastIndex);
	}
	
	public List<T> readAll()
	{
		return new ArrayList<T>(stack);
	}
	
	public void clear()
	{
		stack.clear();
	}
}
