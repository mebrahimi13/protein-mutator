package combination;

import gui.ExecutionCofiguration;

import java.math.BigInteger;

public class MathUtil {
	
	public static BigInteger mutationCount(final ExecutionCofiguration configuration )
	{
		int mutationBegin = configuration.getBegin();
		int mutationEnd = configuration.getEnd();
		int mutationNumber = configuration.getMutationNumber();
		
		BigInteger combinationsCount = MathUtil.combinationCount(mutationEnd-mutationBegin+1, mutationNumber);
		BigInteger mutationsPerCombination = BigInteger.valueOf(19).pow(mutationNumber);
		BigInteger totalGenerated = combinationsCount.multiply(mutationsPerCombination);
		
		return totalGenerated;
	}
	
	private static BigInteger combinationCount(int n, int k)
	{
		return permutationCount(n, k).divide(factorial(k));
	}
	
	private static BigInteger permutationCount(int n, int k)
	{
		BigInteger perm = BigInteger.valueOf(1);
		
		for( int i = n ; i > (n-k) ; i-- )
		{
			perm = perm.multiply(BigInteger.valueOf(i));
		}
		
		return perm;
	}
	
	private static BigInteger factorial(int k)
	{
		BigInteger fact = BigInteger.valueOf(1);
		
		for( int i = k ; i > 1 ; i-- )
		{
			fact = fact.multiply(BigInteger.valueOf(i));
		}
		
		return fact;
	}

}
