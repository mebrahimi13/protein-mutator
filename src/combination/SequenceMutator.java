package combination;

import java.math.BigInteger;
import java.util.List;

import gui.ExecutionCofiguration;
import gui.UIWorker;


public class SequenceMutator 
{
	private UIWorker uiWorker;
	private ExecutionCofiguration configuration;
	private AminoAcidPermutator aminoAcidPermutation;
	private Stack<Integer> stack;
	
	
	public SequenceMutator(UIWorker uiWorker, final ExecutionCofiguration configuration)
	{
		this.uiWorker = uiWorker;
		this.configuration = configuration;
		this.aminoAcidPermutation = new AminoAcidPermutator(configuration);
		this.stack = new Stack<Integer>();
	}
	
	public void compute()
	{
		stack.clear();
		compute(configuration.getBegin(), configuration.getEnd(), configuration.getMutationNumber());
	}
	
	private void compute(int begin, int end, int level)
	{
		if( level > 1)
		{
			handleRecursion(begin, end, level);
		}
		
		else
		{
			handleSimpleCase(begin, end);
		}
	}

	private void handleRecursion(int begin, int end, int level) {
		for(int position = begin; position <= end - (level-1); position++)
		{
			stack.push(position);
			compute(position + 1, end, level - 1);
			stack.pop();
		}
	}

	private void handleSimpleCase(int begin, int end) {
		for(int positionLast = begin; positionLast <= end; positionLast++)
		{
			stack.push(positionLast);
			List<Integer> substitutionLocations = stack.readAll();
			aminoAcidPermutation.permutate(substitutionLocations);
			stack.pop();

			BigInteger progress = BigInteger.valueOf(19).pow(substitutionLocations.size());
			uiWorker.addProgress(progress);
		}
	}

}
