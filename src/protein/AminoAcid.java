package protein;

import java.util.ArrayList;
import java.util.List;

public enum AminoAcid {
	
	A,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	K,
	L,
	M,
	N,
	P,
	Q,
	R,
	S,
	T,
	V,
	W,
	Y;
	
	public static Sequence createAminoAcidSequence(String sequenceString)
	{
		List<AminoAcid> result = new ArrayList<>();
		
		for( int i = 0 ; i < sequenceString.length() ; i++ )
		{
			String aminoAcidCharacter = String.valueOf(sequenceString.charAt(i)).toUpperCase();
			try
			{
				AminoAcid aminoAcid = AminoAcid.valueOf(aminoAcidCharacter);
				result.add(aminoAcid);
			}
			catch(IllegalArgumentException exp)
			{
				throw new IllegalArgumentException("Unknown symbol \"" + aminoAcidCharacter + "\" detected in amino acid sequence.");
			}
		}
		
		return new Sequence(result);
	}
}
