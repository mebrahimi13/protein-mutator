package protein;

import java.util.ArrayList;
import java.util.List;

public final class Sequence {
	
	private List<AminoAcid> sequence;
	
	public Sequence(List<AminoAcid> sequence)
	{
		this.sequence = sequence;
	}
	
	public Sequence(Sequence copySequence)
	{
		this.sequence = new ArrayList<AminoAcid>(copySequence.sequence);
	}
	
	public AminoAcid getAminoAcidAt(final int location)
	{
		return sequence.get(location - 1);
	}
	
	public void substitute(final int location, final AminoAcid mutation)
	{
		this.sequence.set(location - 1, mutation);
	}
	
	@Override
	public String toString() 
	{
		StringBuffer buffer = new StringBuffer();
		for(AminoAcid aminoAcid : sequence )
		{
			buffer.append(aminoAcid);
		}
		return buffer.toString();
	}
}
